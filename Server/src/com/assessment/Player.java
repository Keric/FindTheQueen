package com.assessment;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Player extends Thread {
	final DataInputStream dis;
	final DataOutputStream dos;
	final Socket s;
	String dealer;
	String playername = "";
	private ArrayList<HashMap<String, String>> users;
	private ArrayList<String> authenticatedUsers;

	public Player(Socket s, DataInputStream dis, DataOutputStream dos, ArrayList<HashMap<String, String>> users,
			ArrayList<String> authenticatedUsers, String dealer, String playerTurn, int rounds) {
		this.s = s;
		this.dis = dis;
		this.dos = dos;
		this.users = users;
		this.authenticatedUsers = authenticatedUsers;
		this.dealer = dealer;
	}

	@Override
	public void run() {
		while (true) {
			try {
				authenticate();

			} catch (Exception e) {
				System.out.println(e.getMessage());
				break;
			}
		}

	}

	@SuppressWarnings("finally")
	public boolean authenticate() {
		boolean isAuthenticated = false;
		try {

			while (!isAuthenticated) {
				dos.writeUTF("Username: ");
				String username = dis.readUTF();
				dos.writeUTF("Password: ");
				String password = dis.readUTF();
				isAuthenticated = search(username, password);
				if (isAuthenticated) {
					if (authenticatedUsers.contains(username)) {
						dos.writeUTF("User Already Athenticated");
					} else {
						authenticatedUsers.add(username);
						this.playername = username;
						dos.writeUTF("playername," + username);
						dos.writeUTF("loggedin");
						dos.flush();

						while (true) {
							int sz = authenticatedUsers.size();
							if (sz == 2) {
								dos.writeUTF("Game Started");

								dealer = authenticatedUsers.get(new Random().nextInt(2));

								while (true) {

									if (dealer.equals(playername)) {
										dos.writeUTF("Choose location to hide queen");
										String option = dis.readUTF();

									} else {
										dos.writeUTF("opponents turn");
									}
									Thread.sleep(100);
								}
							}
							Thread.sleep(100);
						}
					}

				} else {
					dos.writeUTF("Authentication Failed");
				}
				Thread.sleep(100);
			}

		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			return isAuthenticated;
		}
	}

	public boolean search(String username, String password) {
		boolean isFound = false;
		for (HashMap<String, String> user : users) {
			if (user.get("username").equals(username) && user.get("password").equals(password)) {
				isFound = true;
				break;
			}
		}
		return isFound;
	}

}
