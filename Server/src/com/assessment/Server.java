package com.assessment;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class Server {
	
	static volatile ArrayList<HashMap<String, String>> users = new ArrayList<>();
	static volatile ArrayList<String> authenticatedUsers;

	static volatile int rounds = 1;
	static volatile String dealer = "";
	static volatile String playerTurn = "";
	public  static void main(String[] args) {
		try {
			authenticatedUsers = new ArrayList<>();
			// CREATE USER PROFILE START
			HashMap<String, String> user1 = new HashMap<>();
			HashMap<String, String> user2 = new HashMap<>();

			user1.put("username", "matty7");
			user1.put("password", "win&win99");
			user2.put("username", "dannyboi");
			user2.put("password", "dre@margh_shelled");

			users.add(user1);
			users.add(user2);
			// CREATE USER PROFILE END

			System.out.println("Server Started");

			// server is listening on port 7621
			@SuppressWarnings("resource")
			ServerSocket ss = new ServerSocket(AppProps.port);

			// running infinite loop for getting
			// client request
			while (true) {
				Socket s = null;

				try {
					// socket object to receive incoming client requests
					s = ss.accept();

					System.out.println("Player Joined Session : " + s);

					// obtaining input and out streams
					DataInputStream dis = new DataInputStream(s.getInputStream());
					DataOutputStream dos = new DataOutputStream(s.getOutputStream());

					// create a new thread object
					Thread t = new Player(s, dis, dos, users, authenticatedUsers, dealer, playerTurn, rounds);

					// Invoking the start() method
					t.start();

				} catch (Exception e) {
					s.close();
					System.out.println(e.getMessage());					
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
