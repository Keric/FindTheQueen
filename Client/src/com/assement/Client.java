package com.assement;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	private static String playerName = "";

	public static void main(String[] args) throws IOException {
		try (Socket s = new Socket(AppProps.host, AppProps.port); Scanner scn = new Scanner(System.in);) {

			DataInputStream dis = new DataInputStream(s.getInputStream());
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());

			// the following loop performs the exchange of
			// information between client and player class
			while (true) {

				String message = dis.readUTF();
				if (message.equals("Dealer is hiding queen!")) {
					System.out.println(message);
				} else if (message.split(",")[0].equals("playername")) {
					playerName = message.split(",")[1];
				} else if (message.equals("opponents turn")) {

				} else if (message.equals("loggedin")) {
					System.out.println("Waiting on player 2!");
				} else if (message.equals("Game Started")) {
				} else if (message.equals("Choose location to hide queen")) {
					System.out.println(message);
					String userInput = scn.nextLine();
					dos.writeUTF(userInput);
				}else if (message.equals("User Already Athenticated")) {
					System.out.println(message);
				}else if (message.equals("Authentication Failed")) {
					System.out.println(message);
				} else {
					System.out.println(message);
					String userInput = scn.nextLine();
					dos.writeUTF(userInput);
				}

				Thread.sleep(100);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
